package in.audr;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestingCompletableFuture {
    private static final String REST_URI = "http://www.mocky.io/v2/";
    private static final String PATH = "5cc0c524310000eb4303653c";

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newWorkStealingPool(64);
        Stream<CompletableFuture<User>> completableFutureStream = IntStream
                .range(0, 1000)
                .mapToObj(id -> getUserAsync(executorService));

        long startTime = System.currentTimeMillis();

        List<CompletableFuture<User>> futures = completableFutureStream.collect(Collectors.toList());

        List<User> users =
                futures.stream()
                        .map(CompletableFuture::join)
                        .peek(System.out::println)
                        .collect(Collectors.toList());

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Users size: "+users.size());
        System.out.println("Executed in: "+elapsedTime+"ms");

        System.exit(0);
    }

    private static CompletableFuture<User> getUserAsync(ExecutorService executorService) {
        return CompletableFuture.supplyAsync(() -> {

            try {
                return ClientBuilder.newClient()
                        .target(REST_URI)
                        .path(PATH)
                        .request(MediaType.APPLICATION_JSON)
                        .get(User.class);
            } catch (Throwable e) {
                System.out.println("Une erreur s'est produite durant l'appel HTTP");
                return null;
            }
        },
        executorService);
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class User {
        String gender;
        String email;
        String phone;

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        @Override
        public String toString() {
            return "{ gender: "+gender+", phone:"+phone+", email:"+email+" }\n";
        }
    }
}
