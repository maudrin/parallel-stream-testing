package in.audr;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestinParallelStream {


    private static final String REST_URI = "http://www.mocky.io/v2/";
    private static final String PATH = "5cc0c524310000eb4303653c";


    public static void main(String[] args) {

        List<String> argsList = Arrays.asList(args);
        IntStream intStream = IntStream.range(1, 1000);
        if(argsList.size()>0 && StringUtils.equalsIgnoreCase("parallel",argsList.get(0))) {
            intStream = intStream.parallel();
        }
        Stream<User> streamOfUser = intStream
                .mapToObj(i -> getUser())
                .filter(Objects::nonNull)
                .peek(System.out::println);


        long startTime = System.currentTimeMillis();

        ForkJoinPool customThreadPool = new ForkJoinPool(256);
        try {
            List<User> users = customThreadPool
                    .submit(() -> streamOfUser.collect(Collectors.toList()))
                    .get();


            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;
            System.out.println("Users size: "+users.size());
            System.out.println("Executed in: "+elapsedTime+"ms");
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static User getUser() {
        try {
            return ClientBuilder.newClient()
                    .target(REST_URI)
                    .path(PATH)
                    .request(MediaType.APPLICATION_JSON)
                    .get(User.class);
        } catch (Throwable e) {
            System.out.println("Une erreur s'est produite durant l'appel HTTP");
            return null;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class User {
        String gender;
        String email;
        String phone;

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        @Override
        public String toString() {
            return "{ gender: "+gender+", phone:"+phone+", email:"+email+" }\n";
        }
    }
}
